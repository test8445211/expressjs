import express from 'express';

const router = express.Router();

let todos = [
    { id: 1, task: 'Eat meal', checked: false },
    { id: 2, task: 'Clean toilet', checked: false }
];

// Get all Todos
router.get('/', (req,res) => {
    res.status(200).send(todos);


})

// Get a single todo using ID
router.get('/:id', (req,res) => {
    const taskId = req.params.id;
    const task = todos.find(todo => todo.id === parseInt(taskId));

    if(task){
        res.status(200).send(task)
    } else {
        res.status(404).json({error: 'Todo not found'})
    }
})

// Create a todo

router.post('/', (req,res) => {
    const { task } = req.body;

    if(!task) {
        res.status(400).json({ error: 'Invalid/Empty request body' });
        return;
    }
    const newTodoItem = { ...req.body, id: new Date().getTime(), checked: false }
    todos = [...todos, newTodoItem]
    res.status(201).send(newTodoItem)
})

// Update a todo check status using ID
router.patch('/:id', (req,res) => {
    const taskId = req.params.id;
    const { checked } = req.body;
    const task = todos.find(todo => todo.id === parseInt(taskId));

    if(typeof checked !== 'boolean') {
        res.status(400).json({ error: 'Invalid request body' });
        return;
    }

    if(task){
        task.checked = checked
        res.status(200).send(task)
    } else {
        res.status(404).json({error: 'Todo not found'})
    }
})

// Delete a todo item
router.delete('/:id', (req,res)=> {
    const taskId = req.params.id;
    const task = todos.find(todo => todo.id === parseInt(taskId));

    if (task !== -1) {
        todos.splice(task, 1);
        res.sendStatus(204);
    } else {
        res.status(404).json({ error: 'Todo not found' });
    }
})

export default router;